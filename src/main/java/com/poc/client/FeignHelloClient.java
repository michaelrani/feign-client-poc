package com.poc.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "simple-client", url = "${hello.endpoint-url}")
public interface FeignHelloClient {
	@GetMapping("/hello/msg") 
	public String getHello();
}


package com.poc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.poc.service.FeignClientService;

@RestController
@RequestMapping("/feign")
public class FeignController {

	@Autowired
	private FeignClientService feignClientService;

    @GetMapping("/msg")
    public String greeting() {
    	System.out.println("*** [FeignController] In... ");
        return feignClientService.getHelloClient();
    }
}

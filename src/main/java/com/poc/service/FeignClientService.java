package com.poc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poc.client.FeignHelloClient;

@Service
public class FeignClientService {
	
	@Autowired
	private FeignHelloClient feignHelloClient;
	public String getHelloClient() {
		return feignHelloClient.getHello();
	}
}

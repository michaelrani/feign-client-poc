package com.poc;

import org.apache.http.HttpStatus;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.tomakehurst.wiremock.http.RequestMethod;

//@RunWith(SpringRunner.class)
//@SpringBootTest(properties = "google.url=http://google.com") // emulate application.properties
//@ContextConfiguration(initializers = FeignHelloClientTest.RandomPortInitializer.class)
//@EnableFeignClients(clients = FeignHelloClientTest.Google.class)
public class FeignHelloClientTest {

//    @ClassRule
//    public static WireMockClassRule wireMockRule = new WireMockClassRule(
//        wireMockConfig().dynamicPort()
//    );
//
//    @FeignClient(name = "google", url = "${google.url}")
//    public interface Google {    
//        @RequestMapping(method = RequestMethod.GET, value = "/")
//        String request();
//    }
//
//    @Autowired
//    public Google google;
//
//    @Test
//    public void testName() throws Exception {
//        stubFor(get(urlEqualTo("/"))
//                .willReturn(aResponse()
//                        .withStatus(HttpStatus.OK.value())
//                        .withBody("Hello")));
//
//        assertEquals("Hello", google.request());
//    }
//
//
//    public static class RandomPortInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
//        @Override
//        public void initialize(ConfigurableApplicationContext applicationContext) {
//
//            // If the next statement is commented out, 
//            // Feign will go to google.com instead of localhost
//            TestPropertySourceUtils
//                .addInlinedPropertiesToEnvironment(applicationContext,
//                    "google.url=" + "http://localhost:" + wireMockRule.port()
//            );
//        }
//    }
}